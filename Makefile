PROJECT_NAME := "go-hello"

.PHONY: build

build:
	@GOOS=linux GOARCH=amd64 go build -o bin/$(PROJECT_NAME)_linux_amd64 -i -v $(CMD)
	@GOOS=darwin GOARCH=amd64 go build -o bin/$(PROJECT_NAME)_darwin_amd64 -i -v $(CMD)
	@GOOS=windows GOARCH=amd64 go build -o bin/$(PROJECT_NAME).exe -i -v $(CMD)

install:
	@go build -o /usr/local/bin/go-hello main.go
	@mkdir -p /usr/local/share/go-hello/templates
	@cp greeting.tmpl.html /usr/local/share/go-hello/templates

remove:
	@rm /usr/local/bin/go-hello
	@rm -rf /usr/local/share/go-hello
package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"strconv"
)

const (
	EnvKeyPort          = "PORT"
	EnvKeyGreeting      = "GREETING"
	EnvKeyTemplateFile  = "TEMPLATE_FILE"
	DefaultPort         = 80
	DefaultGreeting     = "Hello, Go!"
	DefaultTemplateFile = "./greeting.tmpl.html"
)

type templateData struct {
	Message string
}

func greetingHandler(message string, t *template.Template) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		t.Execute(w, templateData{Message: message})
	}
}

func main() {
	port := getEnvInt(EnvKeyPort, DefaultPort)
	message := getStringEnv(EnvKeyGreeting, DefaultGreeting)
	templateFile := getStringEnv(EnvKeyTemplateFile, DefaultTemplateFile)

	tmpl := template.Must(template.ParseFiles(templateFile))

	http.HandleFunc("/", greetingHandler(message, tmpl))
	log.Fatal(http.ListenAndServe(fmt.Sprintf("0.0.0.0:%d", port), nil))
}

func getStringEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func getEnvInt(key string, fallback int) int {
	if value, ok := os.LookupEnv(key); ok {
		p, err := strconv.ParseInt(value, 10, 32)
		if err != nil {
			panic(err)
		}
		return int(p)
	}
	return fallback
}
